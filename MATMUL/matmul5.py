from scipy import sparse as sp
import numpy as np
import dislib as ds
from pycompss.api.api import compss_barrier
import time

def main():
    a = ds.random_array((2048,2048), block_size=(1024,1024))
    b = ds.random_array((2048,2048), block_size=(1024,1024))

    print("Starting")
    s_time = time.time()
    ds.matmul(a,b)
    compss_barrier()
    print("Fit time ", time.time() - s_time)

if __name__ == "__main__":
    main()

