from scipy import sparse as sp
import numpy as np
import dislib as ds
from pycompss.api.api import compss_barrier
import time

def main():
    a = ds.random_array((1500,1500), block_size=(100,100))
    b = ds.random_array((1500,1500), block_size=(100,100))

    print("Starting")
    s_time = time.time()
    ds.matmul(a,b)
    compss_barrier()
    print("Fit time ", time.time() - s_time)

if __name__ == "__main__":
    main()

