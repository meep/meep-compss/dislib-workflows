num_nodes=${1:-2}
export ComputingUnits=${2:-1}
tracing=${3:-false}
script=${4:-kmeansMedium.py}
enqueue_compss \
  --exec_time=200 \
  --tracing=${tracing} \
  --num_nodes=${num_nodes} \
  --worker_working_dir=local_disk \
  --master_working_dir=/tmp/ \
  --worker_in_master_cpus=0 \
  --lang=python \
  --pythonpath=$PYTHONPATH:$PWD \
  --python_interpreter=python3 \
  $PWD/${script}
