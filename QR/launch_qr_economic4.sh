num_nodes=${1:-2}
export ComputingUnits=${2:-1}
export OMP_NUM_THREADS=${2:-1}

enqueue_compss \
  --exec_time=200 \
  -t \
  --num_nodes=${num_nodes} \
  --worker_working_dir=local_disk \
  --worker_in_master_cpus=0 \
  --lang=python \
  --pythonpath=/home/pverges/dislib/:/home/pverges/dislib/tests/performance/mn4/scripts \
  --python_interpreter=python3 \
  qr_economic4.py
