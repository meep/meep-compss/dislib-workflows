import numpy as np
from sklearn.datasets import make_classification
from pycompss.api.api import compss_wait_on
from dislib.classification import CascadeSVM
from pycompss.api.api import compss_barrier
import dislib as ds
import time

def main():
    x, y = make_classification(
        n_samples=15000,
        n_features=30,
        n_classes=2,
        n_informative=8,
        n_redundant=3,
        n_repeated=2,
        n_clusters_per_class=4,
        shuffle=True,
        random_state=0,
    )
    x_train = ds.array(x[::2], (300, 10))
    y_train = ds.array(y[::2][:, np.newaxis], (300, 1))
    x_test = ds.array(x[1::2], (300, 10))
    y_test = ds.array(y[1::2][:, np.newaxis], (300, 1))

    csvm = CascadeSVM(cascade_arity=3, max_iter=5,
                      tol=1e-4, kernel='linear', c=2,
                      check_convergence=True,
                      random_state=666, verbose=False)

    s_time = time.time()
    csvm.fit(x_train, y_train)
    compss_barrier()
    print("Fit time ", time.time() - s_time)


if __name__ == '__main__':
    main()

