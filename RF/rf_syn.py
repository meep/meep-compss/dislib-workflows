import performance
import time
import dislib as ds
from dislib.classification import RandomForestClassifier
from pycompss.api.api import compss_barrier
from sklearn.datasets import make_classification
import numpy as np

def main():
    x, y = make_classification(
        n_samples=10000,
        n_features=20,
        n_classes=3,
        n_informative=4,
        n_redundant=2,
        n_repeated=1,
        n_clusters_per_class=2,
        shuffle=True,
        random_state=0,
    )
    x_train = ds.array(x[::2], (300, 10))
    y_train = ds.array(y[::2][:, np.newaxis], (300, 1))
    x_test = ds.array(x[1::2], (300, 10))
    y_test = ds.array(y[1::2][:, np.newaxis], (300, 1))

    rf = RandomForestClassifier(random_state=0)
    s_time = time.time()
    rf.fit(x_train, y_train)
    compss_barrier()
    print("Fit time ", time.time() - s_time)

if __name__ == "__main__":
    main()

